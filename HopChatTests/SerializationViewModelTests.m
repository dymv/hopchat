//
//  SerializationViewModelTests.m
//  HopChat
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <HopChatCore/HopChatCore.h>
#import "SerializationViewModel.h"

@interface SerializationViewModelTests : XCTestCase

@property (nonatomic, strong) id<MessageSerializer> serializer;
@property (nonatomic, strong) SerializationViewModel* viewModel;

@end

@implementation SerializationViewModelTests

- (void)setUp {
  [super setUp];
  self.serializer = OCMProtocolMock(@protocol(MessageSerializer));
  self.viewModel = [[SerializationViewModel alloc] initWithMessageSerializer:self.serializer];
}

- (void)tearDown {
  [super tearDown];
}

- (void)testNotifiesAboutLoadingStateWhenViewIsReady {
  XCTestExpectation* expectation = [self expectationWithDescription:@"loading state"];
  self.viewModel.loadingStateDidChange = ^(SerializationViewModel* viewModel) {
    [expectation fulfill];
  };
  [self.viewModel viewIsReadyForDisplay];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

- (void)testNotifiesAboutMessageTextWhenViewIsReady {
  XCTestExpectation* expectation = [self expectationWithDescription:@"message text"];
  self.viewModel.messageTextDidChange = ^(SerializationViewModel* viewModel) {
    [expectation fulfill];
  };
  [self.viewModel viewIsReadyForDisplay];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

- (void)testNotifiesAboutOutputTextWhenViewIsReady {
  XCTestExpectation* expectation = [self expectationWithDescription:@"output text"];
  self.viewModel.outputTextDidChange = ^(SerializationViewModel* viewModel) {
    [expectation fulfill];
  };
  [self.viewModel viewIsReadyForDisplay];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

- (void)testNotifiesAboutErrorTextWhenGotError {
  OCMStub([self.serializer serialize:[OCMArg any] withCompletion:[OCMArg any]]).andDo(^(NSInvocation* invocation) {
    MessageSerializerCompletion completion;
    [invocation getArgument:&completion atIndex:3];
    completion(nil, [NSError errorWithDomain:@"TestDomain" code:1 userInfo:nil]);
  });

  XCTestExpectation* expectation = [self expectationWithDescription:@"error text"];
  self.viewModel.errorTextDidChange = ^(SerializationViewModel* viewModel) {
    [expectation fulfill];
  };

  [self.viewModel sendMessage:@"Message"];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

- (void)testStartsLoadingOnSendMessage {
  XCTestExpectation* expectation = [self expectationWithDescription:@"loading state"];
  self.viewModel.loadingStateDidChange = ^(SerializationViewModel* viewModel) {
    if ([viewModel isLoading]) {
      [expectation fulfill];
    }
  };
  [self.viewModel sendMessage:@"Message"];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

- (void)testStopsLoadingOnSerializationComplete {
  OCMStub([self.serializer serialize:[OCMArg any] withCompletion:[OCMArg any]]).andDo(^(NSInvocation* invocation) {
    MessageSerializerCompletion completion;
    [invocation getArgument:&completion atIndex:3];
    completion(@"Serialized", nil);
  });

  XCTestExpectation* expectation = [self expectationWithDescription:@"loading state"];
  self.viewModel.loadingStateDidChange = ^(SerializationViewModel* viewModel) {
    if (![viewModel isLoading]) {
      [expectation fulfill];
    }
  };

  [self.viewModel sendMessage:@"Message"];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

- (void)testSerializesOnSendMessage {
  NSString* message = @"Message";
  [self.viewModel sendMessage:message];
  OCMVerify([self.serializer serialize:message withCompletion:[OCMArg any]]);
}

- (void)testSetsOutputTextOnSerializationComplete {
  OCMStub([self.serializer serialize:[OCMArg any] withCompletion:[OCMArg any]]).andDo(^(NSInvocation* invocation) {
    MessageSerializerCompletion completion;
    [invocation getArgument:&completion atIndex:3];
    completion(@"Serialized", nil);
  });

  XCTestExpectation* expectation = [self expectationWithDescription:@"output text"];
  self.viewModel.outputTextDidChange = ^(SerializationViewModel* viewModel) {
    [expectation fulfill];
  };

  [self.viewModel sendMessage:@"Message"];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

- (void)testNotifiesAboutErrorEvenIfPreviousErrorHadSameDescription {
  __block BOOL forceError = YES;
  OCMStub([self.serializer serialize:[OCMArg any] withCompletion:[OCMArg any]]).andDo(^(NSInvocation* invocation) {
    MessageSerializerCompletion completion;
    [invocation getArgument:&completion atIndex:3];
    if (forceError) {
      completion(nil, [NSError errorWithDomain:@"TestDomain" code:1 userInfo:@{NSLocalizedDescriptionKey:@"desc"}]);
    } else {
      completion(@"Serialized", nil);
    }
  });

  XCTestExpectation* expectation = [self expectationWithDescription:@"error text"];
  self.viewModel.errorTextDidChange = ^(SerializationViewModel* viewModel) {
    [expectation fulfill];
  };

  [self.viewModel sendMessage:@"Message"];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];

  forceError = NO;
  expectation = [self expectationWithDescription:@"output text"];
  self.viewModel.outputTextDidChange = ^(SerializationViewModel* viewModel) {
    [expectation fulfill];
  };

  [self.viewModel sendMessage:@"Message"];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];

  forceError = YES;
  expectation = [self expectationWithDescription:@"error text"];
  self.viewModel.errorTextDidChange = ^(SerializationViewModel* viewModel) {
    [expectation fulfill];
  };

  [self.viewModel sendMessage:@"Message"];
  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

@end
