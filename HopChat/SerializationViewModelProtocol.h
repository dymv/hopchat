//
//  SerializationViewModelProtocol.h
//  HopChat
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SerializationViewModelProtocol;

NS_ASSUME_NONNULL_BEGIN

typedef void (^SerializationViewModelBlock)(id<SerializationViewModelProtocol> viewModel);

@protocol SerializationViewModelProtocol <NSObject>

@property (nonatomic, assign, readonly, getter=isLoading) BOOL loading;
@property (nonatomic, copy, readonly, nullable) NSString* messageText;
@property (nonatomic, copy, readonly, nullable) NSString* outputText;
@property (nonatomic, copy, readonly, nullable) NSString* errorText;

@property (nonatomic, copy, nullable) SerializationViewModelBlock loadingStateDidChange;
@property (nonatomic, copy, nullable) SerializationViewModelBlock messageTextDidChange;
@property (nonatomic, copy, nullable) SerializationViewModelBlock outputTextDidChange;
@property (nonatomic, copy, nullable) SerializationViewModelBlock errorTextDidChange;

- (void)viewIsReadyForDisplay;
- (void)sendMessage:(NSString*)message;

@end

NS_ASSUME_NONNULL_END
