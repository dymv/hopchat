//
//  SerializationViewModel.h
//  HopChat
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SerializationViewModelProtocol.h"

@protocol MessageSerializer;

NS_ASSUME_NONNULL_BEGIN

@interface SerializationViewModel : NSObject <SerializationViewModelProtocol>

- (instancetype)initWithMessageSerializer:(id<MessageSerializer>)serializer NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
