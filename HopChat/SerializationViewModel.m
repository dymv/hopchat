//
//  SerializationViewModel.m
//  HopChat
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <HopChatCore/HopChatCore.h>
#import "SerializationViewModel.h"

@interface SerializationViewModel ()

@property (nonatomic, strong) id<MessageSerializer> serializer;
@property (nonatomic, assign, readwrite, getter=isLoading) BOOL loading;
@property (nonatomic, copy, readwrite) NSString* outputText;

@end

@implementation SerializationViewModel

@synthesize loading = _loading;
@synthesize loadingStateDidChange;

@synthesize messageText = _messageText;
@synthesize messageTextDidChange;

@synthesize outputText = _outputText;
@synthesize outputTextDidChange;

@synthesize errorText = _errorText;
@synthesize errorTextDidChange;

#pragma mark - Object Lifecycle

- (instancetype)init {
  return [self initWithMessageSerializer:[[MessageSerializer alloc] init]];
}

- (instancetype)initWithMessageSerializer:(id<MessageSerializer>)serializer {
  self = [super init];
  if (self) {
    _serializer = serializer;
  }
  return self;
}

#pragma mark - Properties

- (void)setLoading:(BOOL)loading {
  if (_loading == loading) {
    return;
  }
  _loading = loading;
  [self notifyView_loadingStateDidChange];
}

- (void)setMessageText:(NSString*)messageText {
  if (_messageText == messageText) {
    return;
  }
  _messageText = messageText;
  [self notifyView_messageTextDidChange];
}

- (void)setOutputText:(NSString*)outputText {
  if (_outputText == outputText) {
    return;
  }
  _outputText = outputText;
  _errorText = nil;
  [self notifyView_outputTextDidChange];
}

- (void)setErrorText:(NSString*)errorText {
  if (_errorText == errorText) {
    return;
  }
  _errorText = errorText;
  _outputText = nil;
  [self notifyView_errorTextDidChange];
}

#pragma mark - SerializationViewModelProtocol

- (void)viewIsReadyForDisplay {
  [self notifyView_messageTextDidChange];
  [self notifyView_outputTextDidChange];
  [self notifyView_loadingStateDidChange];
}

- (void)notifyView_outputTextDidChange {
  if (self.outputTextDidChange) {
    self.outputTextDidChange(self);
  }
}

- (void)notifyView_errorTextDidChange {
  if (self.errorTextDidChange) {
    self.errorTextDidChange(self);
  }
}

- (void)notifyView_messageTextDidChange {
  if (self.messageTextDidChange) {
    self.messageTextDidChange(self);
  }
}

- (void)notifyView_loadingStateDidChange {
  if (self.loadingStateDidChange) {
    self.loadingStateDidChange(self);
  }
}

- (void)sendMessage:(NSString*)message {
  self.loading = YES;

  [self.serializer serialize:message withCompletion:^(NSString* serialized, NSError* error) {
    [NSOperationQueue.mainQueue addOperationWithBlock:^{
      self.loading = NO;
      if (error) {
        self.errorText = error.localizedDescription;
      } else {
        self.outputText = serialized;
      }
    }];
  }];
}

@end
