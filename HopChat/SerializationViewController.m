//
//  SerializationViewController.m
//  HopChat
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "SerializationViewController.h"
#import "SerializationViewModelProtocol.h"

@interface SerializationViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint* textContainerBottomConstraint;
@property (weak, nonatomic) IBOutlet UITextView* messageTextView;
@property (weak, nonatomic) IBOutlet UITextView* outputTextView;
@property (weak, nonatomic) IBOutlet UIView* outputTextViewBlind;
@property (weak, nonatomic) IBOutlet UIButton* sendButton;

@end

@implementation SerializationViewController

#pragma mark - Object Lifecycle

- (void)dealloc {
  [NSNotificationCenter.defaultCenter removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
  [self unbind];
}

- (void)unbind {
  self.viewModel.outputTextDidChange = nil;
  self.viewModel.messageTextDidChange = nil;
  self.viewModel.loadingStateDidChange = nil;
}

#pragma mark - Properties

- (void)setViewModel:(id<SerializationViewModelProtocol>)viewModel {
  if (_viewModel == viewModel) {
    return;
  }
  _viewModel = viewModel;
  [self bind];
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];

  self.messageTextView.layer.cornerRadius = 3;

  [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];

  [self.viewModel viewIsReadyForDisplay];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [self.messageTextView becomeFirstResponder];
}

- (void)bind {
  __weak typeof(self) weakSelf = self;

  self.viewModel.outputTextDidChange = ^(id<SerializationViewModelProtocol> viewModel) {
    __strong typeof(weakSelf) strongSelf = weakSelf;
    [strongSelf moveToSuccessState];
    strongSelf.outputTextView.text = viewModel.outputText;
  };

  self.viewModel.messageTextDidChange = ^(id<SerializationViewModelProtocol> viewModel) {
    __strong typeof(weakSelf) strongSelf = weakSelf;
    strongSelf.messageTextView.text = viewModel.messageText;
  };

  self.viewModel.loadingStateDidChange = ^(id<SerializationViewModelProtocol> viewModel) {
    __strong typeof(weakSelf) strongSelf = weakSelf;
    if ([viewModel isLoading]) {
      [strongSelf moveToLoadingState];
    } else {
      [strongSelf moveToIdleState];
    }
  };

  self.viewModel.errorTextDidChange = ^(id<SerializationViewModelProtocol> viewModel) {
    __strong typeof(weakSelf) strongSelf = weakSelf;
    [strongSelf moveToErrorState];
    strongSelf.outputTextView.text = viewModel.errorText;
  };
}

- (void)moveToLoadingState {
  [self moveBlindToHiddenState:NO];
  self.sendButton.enabled = NO;
}

- (void)moveToIdleState {
  [self moveBlindToHiddenState:YES];
  self.sendButton.enabled = YES;
}

- (void)moveBlindToHiddenState:(BOOL)hidden {
  CGFloat alphaValue = hidden ? 0.0 : 0.75;
  [UIView animateWithDuration:0.2
                        delay:0.0
                      options:UIViewAnimationOptionBeginFromCurrentState
                   animations:^{
                     self.outputTextViewBlind.alpha = alphaValue;
                   } completion:nil];
}

- (void)moveToErrorState {
  self.outputTextView.textColor = [UIColor redColor];
}

- (void)moveToSuccessState {
  self.outputTextView.textColor = [UIColor blackColor];
}

#pragma mark - Notifications

- (void)keyboardNotification:(NSNotification*)notification {
  NSDictionary* userInfo = notification.userInfo;

  CGRect endFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
  NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
  UIViewAnimationOptions options = [userInfo[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue] << 16 | UIViewAnimationOptionBeginFromCurrentState;

  BOOL isHiding = CGRectGetMinY(endFrame) >= CGRectGetHeight([UIScreen mainScreen].bounds);
  self.textContainerBottomConstraint.constant = isHiding ? 0 : CGRectGetHeight(endFrame);

  [UIView animateWithDuration:duration delay:0 options:options animations:^{
    [self.view layoutIfNeeded];
  } completion:nil];
}

#pragma mark - Interactions

- (IBAction)sendButtonDidTap:(id)sender {
  [self.viewModel sendMessage:self.messageTextView.text];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  if ([self.messageTextView isFirstResponder]) {
    [self.messageTextView resignFirstResponder];
  }
}

@end
