//
//  SerializationViewController.h
//  HopChat
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SerializationViewModelProtocol;

NS_ASSUME_NONNULL_BEGIN

@interface SerializationViewController : UIViewController

@property (nonatomic, strong) id<SerializationViewModelProtocol> viewModel;

@end

NS_ASSUME_NONNULL_END

