//
//  AppDelegate.m
//  HopChat
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "AppDelegate.h"
#import "SerializationViewController.h"
#import "SerializationViewModel.h"

@interface AppDelegate ()

@property (nonatomic, readonly, strong) SerializationViewController* serializationViewController;
@property (nonatomic, readonly, strong) id<SerializationViewModelProtocol> serializationViewModel;

@end

@implementation AppDelegate

@synthesize serializationViewModel = _serializationViewModel;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  self.serializationViewController.viewModel = self.serializationViewModel;
  return YES;
}

- (id<SerializationViewModelProtocol>)serializationViewModel {
  if (!_serializationViewModel) {
    _serializationViewModel = [[SerializationViewModel alloc] init];
  }
  return _serializationViewModel;
}

- (SerializationViewController *)serializationViewController {
  UIViewController* rootViewController = self.window.rootViewController;

  if ([rootViewController isKindOfClass:UINavigationController.class]) {
    UINavigationController* navigationController = (UINavigationController*)rootViewController;
    rootViewController = navigationController.visibleViewController;
  }

  return (SerializationViewController*)rootViewController;
}

@end
