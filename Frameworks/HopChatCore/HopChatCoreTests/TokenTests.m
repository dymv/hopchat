//
//  TokenTests.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/2/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "BaseToken.h"
#import "MentionToken.h"
#import "EmoticonToken.h"
#import "LinkToken.h"

@interface TokenTests : XCTestCase

@end

@implementation TokenTests

- (void)testTokensReturnRegularExpression {
  XCTAssertNotNil(MentionToken.regularExpression);
  XCTAssertNotNil(EmoticonToken.regularExpression);
  XCTAssertNotNil(LinkToken.regularExpression);
}

- (void)testTokensHaveExpectedCategories {
  XCTAssert([MentionToken.category isEqualToString:@"mentions"]);
  XCTAssert([EmoticonToken.category isEqualToString:@"emoticons"]);
  XCTAssert([LinkToken.category isEqualToString:@"links"]);
}

- (void)testTokensHaveExpectedTypes {
  XCTAssertEqual(MentionToken.type, MessageTokenTypeText);
  XCTAssertEqual(EmoticonToken.type, MessageTokenTypeText);
  XCTAssertEqual(LinkToken.type, MessageTokenTypeLink);
}

@end
