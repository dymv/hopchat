//
//  MessageSerializerTests.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "MessageSerializer.h"
#import "URLInfoFetcher.h"
#import "MessageTokenizer.h"
#import "LinkToken.h"
#import "MentionToken.h"
#import "EmoticonToken.h"
#import "URLInfo.h"

@interface MessageSerializerTests : XCTestCase

@property (nonatomic, strong) id<MessageTokenizer> tokenizer;
@property (nonatomic, strong) id<URLInfoFetcher> urlInfoFetcher;
@property (nonatomic, strong) MessageSerializer* serializer;

@end

@implementation MessageSerializerTests

- (void)setUp {
  [super setUp];
  self.tokenizer = OCMProtocolMock(@protocol(MessageTokenizer));
  self.urlInfoFetcher = OCMProtocolMock(@protocol(URLInfoFetcher));
  self.serializer = [[MessageSerializer alloc] initWithTokenizer:self.tokenizer
                                                  urlInfoFetcher:self.urlInfoFetcher];
}

- (void)tearDown {
  [super tearDown];
}

- (void)testTokenizes {
  NSString* message = @"Message";
  XCTestExpectation* expectation = [self expectationWithDescription:@"serialized"];
  [self.serializer serialize:message withCompletion:^(NSString *serialized, NSError* error) {
    [expectation fulfill];
  }];

  [self waitForExpectationsWithTimeout:0.5 handler:nil];

  OCMVerify([self.tokenizer tokenize:message]);
}

- (void)testFetchesInfoForLinkTokens {
  NSString* message = @"Message";
  NSURL* url = [NSURL URLWithString:@"https://apple.com"];
  NSArray* tokens = @[ [[LinkToken alloc] initWithRange:NSMakeRange(0, 1)
                                             rawContent:@"apple.com"
                                                content:url.absoluteString] ];
  OCMStub([self.tokenizer tokenize:message]).andReturn(tokens);

  XCTestExpectation* expectation = [self expectationWithDescription:@"serialized"];
  [self.serializer serialize:message withCompletion:^(NSString *serialized, NSError* error) {
    [expectation fulfill];
  }];

  [self waitForExpectationsWithTimeout:0.5 handler:nil];

  OCMVerify([self.urlInfoFetcher fetchInfoForURL:url error:(NSError*__autoreleasing*)[OCMArg anyPointer]]);
}

- (void)testReturnsSerializedJSONString {
  NSString* message = @"Message";
  NSURL* url = [NSURL URLWithString:@"https://apple.com"];
  NSArray* tokens = @[
    [[LinkToken alloc] initWithRange:NSMakeRange(0, 1) rawContent:@"apple.com" content:url.absoluteString],
    [[MentionToken alloc] initWithRange:NSMakeRange(1, 2) rawContent:@"@Neo" content:@"Neo"],
    [[EmoticonToken alloc] initWithRange:NSMakeRange(1, 2) rawContent:@"white rabbit"],
  ];
  OCMStub([self.tokenizer tokenize:message]).andReturn(tokens);

  URLInfo* urlInfo = [[URLInfo alloc] initWithTitle:@"Title"];
  OCMStub([self.urlInfoFetcher fetchInfoForURL:url error:(NSError*__autoreleasing*)[OCMArg anyPointer]]).andReturn(urlInfo);

  XCTestExpectation* expectation = [self expectationWithDescription:@"serialized"];
  [self.serializer serialize:message withCompletion:^(NSString *serialized, NSError* error) {
    NSDictionary* deserialized = [NSJSONSerialization JSONObjectWithData:[serialized dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    XCTAssertEqualObjects(deserialized[@"emoticons"][0], @"white rabbit");
    XCTAssertEqualObjects(deserialized[@"links"][0][@"title"], @"Title");
    XCTAssertEqualObjects(deserialized[@"links"][0][@"url"], @"https://apple.com");
    XCTAssertEqualObjects(deserialized[@"mentions"][0], @"Neo");
    [expectation fulfill];
  }];

  [self waitForExpectationsWithTimeout:0.5 handler:nil];
}

@end
