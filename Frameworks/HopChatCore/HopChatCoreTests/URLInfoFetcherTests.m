//
//  URLInfoFetcherTests.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "URLInfo.h"
#import "URLInfoFetcher.h"
#import "URLContentsFetcher.h"

@interface URLInfoFetcherTests : XCTestCase

@property (nonatomic, strong) id<URLContentsFetcher> mockContentsFetcher;
@property (nonatomic, strong) URLInfoFetcher* infoFetcher;
@property (nonatomic, copy) NSString* largePageString;

@end

@implementation URLInfoFetcherTests

- (void)setUp {
  [super setUp];
  self.mockContentsFetcher = OCMProtocolMock(@protocol(URLContentsFetcher));
  self.infoFetcher = [[URLInfoFetcher alloc] initWithContentsFetcher:self.mockContentsFetcher];
  NSString* path = [[NSBundle bundleForClass:self.class] pathForResource:@"large-page" ofType:@"txt"];
  self.largePageString = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

- (void)tearDown {
  [super tearDown];
}

- (void)testFetcherPerformance {
    [self measureBlock:^{
        [self assertTitle:@"Atlassian | Software Development and Collaboration Tools"
             fromContents:self.largePageString];
    }];
}

- (void)testTitleParsingInSimpleCase {
  [self assertTitle:@"Title" fromContents:@"<title>Title</title>"];
}

- (void)testReturnNilWhenNoTitle {
  [self assertTitle:nil fromContents:@"<html></html>"];
}

- (void)testParsesTitleWhenInsideOtherTags {
  [self assertTitle:@"Title Inside" fromContents:@"<html><title>Title Inside</title></html>"];
}

- (void)testParsesTitleWhenOpenTagHasAttributes {
  [self assertTitle:@"Title Attributed" fromContents:@"<html><title id=\"MyId\">Title Attributed</title></html>"];
}

- (void)testParsesTitleWithVariousCharacters {
  [self assertTitle:@">.<" fromContents:@"<html><title id=\"MyId\">>.<</title></html>"];
}

- (void)testTrimsWhitespacesAndNewlinesFromTitle {
  [self assertTitle:@"Clean Title" fromContents:@"<html><title>\n  Clean Title  \n</title></html>"];
}

- (void)assertTitle:(NSString*)title fromContents:(NSString*)contents {
  NSURL* url = [NSURL URLWithString:@"http://testurl.com"];
  OCMStub([self.mockContentsFetcher fetchContentsForURL:url error:NULL]).andReturn(contents);
  NSString* fetchedTitle = [self.infoFetcher fetchInfoForURL:url error:NULL].title;
  XCTAssertEqualObjects(title, fetchedTitle);
}

@end
