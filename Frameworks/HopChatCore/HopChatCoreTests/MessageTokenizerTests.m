//
//  MessageTokenizerTests.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "MentionToken.h"
#import "EmoticonToken.h"
#import "LinkToken.h"
#import "MessageTokenizer.h"

@interface MessageTokenizerTests : XCTestCase

@property (nonatomic, strong) MessageTokenizer* tokenizer;
@property (nonatomic, copy) NSString* largeMessageString;

@end

@implementation MessageTokenizerTests

- (void)setUp {
  [super setUp];
  NSArray* tokenClasses = @[MentionToken.class, EmoticonToken.class, LinkToken.class];
  self.tokenizer = [[MessageTokenizer alloc] initWithTokenClasses:tokenClasses];
  NSString* path = [[NSBundle bundleForClass:self.class] pathForResource:@"large-message" ofType:@"txt"];
  self.largeMessageString = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
}

- (void)tearDown {
  [super tearDown];
}

- (void)testTokenizerPerformance {
  [self measureBlock:^{
    NSArray* tokens = [self.tokenizer tokenize:self.largeMessageString];
    XCTAssertEqual(tokens.count, 1450);
  }];
}

- (void)testReturnsEmptyTokensArrayIfInitializedWithNoTokenClasses {
  MessageTokenizer* emptyTokenizer = [[MessageTokenizer alloc] initWithTokenClasses:@[]];
  NSArray* tokens = [emptyTokenizer tokenize:@"Hello, @bob, how are you? (smile)"];
  XCTAssert(tokens.count == 0);
}

- (void)testTokenizesMention {
  NSArray* tokens = [self.tokenizer tokenize:@"Hello, @bob!@bob bob@bob @bob@bob 123@bob"];
  [self assertTokens:tokens equal:@[
    @[@"bob", @"@bob", @7, @4],
    @[@"bob", @"@bob", @12, @4],
    @[@"bob", @"@bob", @25, @4]
  ]];
}

- (void)testDoesNotTokenizeIncorrectMention {
  NSArray* tokens = [self.tokenizer tokenize:@"some@bobcom 2@possiblybob@notbob"];
  XCTAssertEqual(tokens.count, 0);
}

- (void)testTokenizesEmoticons {
  NSArray* tokens = [self.tokenizer tokenize:@"(smile1) (not(smile2)not) (looooooongsmile)"];
  [self assertTokens:tokens equal:@[
    @[@"smile1", @"smile1", @1, @6],
    @[@"smile2", @"smile2", @14, @6],
    @[@"looooooongsmile", @"looooooongsmile", @27, @15]
  ]];
}

- (void)testDoesNotTokenizeIncorrectEmoticon {
  NSArray* tokens = [self.tokenizer tokenize:@"(toolooooongsmile) (characters?!) () (())"];
  XCTAssertEqual(tokens.count, 0);
}

- (void)testTokenizesLinks {
  NSArray* tokens =
    [self.tokenizer tokenize:@"https://atlassian.com https://bitbucket.org/dashboard/overview apple.com fb.com"];
  [self assertTokens:tokens equal:@[
    @[@"https://atlassian.com", @"https://atlassian.com", @0, @21],
    @[@"https://bitbucket.org/dashboard/overview", @"https://bitbucket.org/dashboard/overview", @22, @40],
    @[@"http://apple.com", @"apple.com", @63, @9],
    @[@"http://fb.com", @"fb.com", @73, @6],
  ]];
}

- (void)testDoesNotTokenizeIncorrectLinks {
  NSArray* tokens = [self.tokenizer tokenize:@"http:\address.not fancy™character.¢øµ mystrage?site.wow"];
  XCTAssertEqual(tokens.count, 0);
}

#pragma mark - Helpers

- (void)assertTokens:(NSArray*)tokens equal:(NSArray*)expectedTokens {
  XCTAssertEqual(tokens.count, expectedTokens.count);
  [tokens enumerateObjectsUsingBlock:^(id<Token> token, NSUInteger idx, BOOL* stop) {
    NSArray* expectedTokenValues = expectedTokens[idx];
    XCTAssert([token.content isEqualToString:expectedTokenValues[0]]);
    XCTAssert([token.rawContent isEqualToString:expectedTokenValues[1]]);
    XCTAssert(token.range.location == [expectedTokenValues[2] unsignedIntegerValue]);
    XCTAssert(token.range.length == [expectedTokenValues[3] unsignedIntegerValue]);
  }];
}

@end
