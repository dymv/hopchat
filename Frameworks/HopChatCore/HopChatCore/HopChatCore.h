//
//  HopChatCore.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HopChatCore.
FOUNDATION_EXPORT double HopChatCoreVersionNumber;

//! Project version string for HopChatCore.
FOUNDATION_EXPORT const unsigned char HopChatCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HopChatCore/PublicHeader.h>

#import <HopChatCore/MessageSerializer.h>
