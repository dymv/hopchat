//
//  URLContentsFetcher.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol URLContentsFetcher <NSObject>

- (NSString* _Nullable)fetchContentsForURL:(NSURL*)url error:(NSError* __autoreleasing * _Nullable)error;

@end

@interface URLContentsFetcher : NSObject <URLContentsFetcher>

@end

NS_ASSUME_NONNULL_END
