//
//  URLInfoFetcher.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/2/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "URLInfoFetcher.h"
#import "URLInfo.h"
#import "URLContentsFetcher.h"

@interface URLInfoFetcher ()

@property (nonatomic, strong) id<URLContentsFetcher> contentsFetcher;

@end

@implementation URLInfoFetcher

#pragma mark - Object Lifecycle

- (instancetype)init {
  URLContentsFetcher* contentsFetcher = [[URLContentsFetcher alloc] init];
  return [self initWithContentsFetcher:contentsFetcher];
}

- (instancetype)initWithContentsFetcher:(id<URLContentsFetcher>)contentsFetcher {
  self = [super init];
  if (self) {
    _contentsFetcher = contentsFetcher;
  }
  return self;
}

#pragma mark - Public Interface

- (URLInfo*)fetchInfoForURL:(NSURL*)url error:(NSError* __autoreleasing *)error {
  NSParameterAssert(url);
  NSString* contents = [self.contentsFetcher fetchContentsForURL:url error:error];
  URLInfo* info = [[URLInfo alloc] initWithTitle:[self titleFromURLContents:contents]];
  return info;
}

#pragma mark - Private

- (NSString*)titleFromURLContents:(NSString*)contents {
  if (!contents) {
    return nil;
  }
  NSScanner* scanner = [NSScanner scannerWithString:contents];
  scanner.charactersToBeSkipped = nil;

  [scanner scanUpToString:@"<title" intoString:NULL]; // separating tag to support tags like this <title id="titleID">
  if ([scanner isAtEnd]) {
    return nil;
  }
  [scanner scanUpToString:@">" intoString:NULL];
  NSUInteger titleOpenTagEndLocation = scanner.scanLocation;
  NSUInteger titleLocation = titleOpenTagEndLocation + 1; // 1 is the length of '>'

  [scanner scanUpToString:@"</title>" intoString:NULL];
  NSUInteger titleCloseTagLocation = scanner.scanLocation;

  NSString* title = [contents substringWithRange:NSMakeRange(titleLocation, titleCloseTagLocation - titleLocation)];

  return [title stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
