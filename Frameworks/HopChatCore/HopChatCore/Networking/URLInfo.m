//
//  URLInfo.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/2/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "URLInfo.h"

@implementation URLInfo

- (instancetype)initWithTitle:(NSString*)title {
  self = [super init];
  if (self) {
    _title = [title copy];
  }
  return self;
}

@end
