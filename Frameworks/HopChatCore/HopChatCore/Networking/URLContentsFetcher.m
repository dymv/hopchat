//
//  URLContentsFetcher.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "URLContentsFetcher.h"

@implementation URLContentsFetcher

+ (NSCache*)sharedContentsCache {
  static NSCache* cache;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    cache = [[NSCache alloc] init];
    cache.countLimit = 25;
    cache.totalCostLimit = 5 * 1024 * 1024;
  });
  return cache;
}

- (NSString*)fetchContentsForURL:(NSURL*)url error:(NSError* __autoreleasing *)outError {
  NSString* cached = [[self.class sharedContentsCache] objectForKey:url.absoluteString];
  if (cached) {
    return cached;
  }
  NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL:url];
  urlRequest.timeoutInterval = 10;
  urlRequest.cachePolicy = NSURLRequestReturnCacheDataElseLoad;

  dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);

  NSURLSessionConfiguration* configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession* session = [NSURLSession sessionWithConfiguration:configuration];

  __block NSString* contents;
  __block NSError* dataTaskError;
  [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
    if (error) {
      dataTaskError = [NSError errorWithDomain:error.domain code:error.code userInfo:error.userInfo];
    } else if (data) {
      contents = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
      if (contents) {
        [[self.class sharedContentsCache] setObject:contents forKey:url.absoluteString cost:data.length];
      }
    }
    dispatch_semaphore_signal(semaphore);
  }] resume];

  dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

  if (outError) {
    *outError = dataTaskError;
  }

  return contents;
}

@end
