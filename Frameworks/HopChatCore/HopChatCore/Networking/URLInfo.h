//
//  URLInfo.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/2/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface URLInfo : NSObject

@property (nonatomic, copy, readonly) NSString* title;

- (instancetype)initWithTitle:(NSString*)title;

@end

NS_ASSUME_NONNULL_END
