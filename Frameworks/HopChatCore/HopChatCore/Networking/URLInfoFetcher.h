//
//  URLInfoFetcher.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/2/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

@class URLInfo;
@protocol URLContentsFetcher;

NS_ASSUME_NONNULL_BEGIN

@protocol URLInfoFetcher <NSObject>

- (URLInfo*)fetchInfoForURL:(NSURL*)url error:(NSError* __autoreleasing * _Nullable)error;

@end

@interface URLInfoFetcher : NSObject <URLInfoFetcher>

- (instancetype)initWithContentsFetcher:(id<URLContentsFetcher>)contentsFetcher NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
