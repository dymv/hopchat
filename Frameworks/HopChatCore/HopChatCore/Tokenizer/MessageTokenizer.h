//
//  MessageTokenizer.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Token;

NS_ASSUME_NONNULL_BEGIN

typedef void(^MessageTokenizerCompletion)();

@protocol MessageTokenizer <NSObject>

- (NSArray<id<Token>>*)tokenize:(NSString*)message;

@end

@interface MessageTokenizer : NSObject <MessageTokenizer>

@property (nonatomic, copy, readonly) NSArray<Class>* tokenClasses;
- (instancetype)initWithTokenClasses:(NSArray<Class>*)tokenClasses NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
