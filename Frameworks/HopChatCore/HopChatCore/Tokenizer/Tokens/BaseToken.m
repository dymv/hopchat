//
//  BaseToken.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "BaseToken.h"

@implementation BaseToken

@synthesize range = _range;
@synthesize content = _content;
@synthesize rawContent = _rawContent;

+ (MessageTokenType)type {
  return MessageTokenTypeNA;
}

+ (NSString *)category {
  return nil;
}

+ (NSString *)regularExpressionString {
  return nil;
}

+ (NSRegularExpression*)regularExpression {
  return [NSRegularExpression regularExpressionWithPattern:[self regularExpressionString] options:0 error:NULL];
}

- (instancetype)initWithRange:(NSRange)range rawContent:(NSString*)rawContent {
  return [self initWithRange:range rawContent:rawContent content:rawContent];
}

- (instancetype)initWithRange:(NSRange)range rawContent:(NSString*)rawContent content:(NSString*)content {
  self = [super init];
  if (self) {
    _range = range;
    _rawContent = [rawContent copy];
    _content = content ? [content copy] : _rawContent;
  }
  return self;
}

- (NSString*)description {
  return [NSString stringWithFormat:@"<%@ %p: %@ %@ %@>", NSStringFromClass(self.class), self, self.class.category, NSStringFromRange(self.range), self.content];
}

@end
