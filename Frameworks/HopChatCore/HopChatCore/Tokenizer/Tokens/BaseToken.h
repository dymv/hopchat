//
//  BaseToken.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MessageTokenType) {
  MessageTokenTypeNA,
  MessageTokenTypeText,
  MessageTokenTypeLink
};

NS_ASSUME_NONNULL_BEGIN

@protocol Token <NSObject>

@property (nonatomic, readonly, assign) NSRange range;
@property (nonatomic, readonly, copy) NSString* content;
@property (nonatomic, readonly, copy) NSString* rawContent;

@end

@interface BaseToken : NSObject <Token>

+ (MessageTokenType)type;
+ (NSString*)category;
+ (NSRegularExpression*)regularExpression;

- (instancetype)initWithRange:(NSRange)range rawContent:(NSString*)rawContent;
- (instancetype)initWithRange:(NSRange)range rawContent:(NSString*)rawContent content:(NSString*)content;

@end

NS_ASSUME_NONNULL_END
