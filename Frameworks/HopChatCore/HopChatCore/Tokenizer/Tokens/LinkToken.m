//
//  LinkToken.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "LinkToken.h"
#import "Constants.h"

@implementation LinkToken

+ (MessageTokenType)type {
  return MessageTokenTypeLink;
}

+ (NSString *)category {
  return HopChatCoreLinksKeyString;
}

+ (NSRegularExpression*)regularExpression {
  return [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
}

+ (NSString*)regularExpressionString {
  return nil;
}

@end
