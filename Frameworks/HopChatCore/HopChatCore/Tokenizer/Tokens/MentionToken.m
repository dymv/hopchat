//
//  MentionToken.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "MentionToken.h"
#import "Constants.h"

@implementation MentionToken

+ (MessageTokenType)type {
  return MessageTokenTypeText;
}

+ (NSString *)category {
  return HopChatCoreMentionsKeyString;
}

+ (NSString*)regularExpressionString {
  return @"(?<=(?:^|\\W))@\\w+";
}

@end
