//
//  EmoticonToken.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "EmoticonToken.h"
#import "Constants.h"

@implementation EmoticonToken

+ (MessageTokenType)type {
  return MessageTokenTypeText;
}

+ (NSString *)category {
  return HopChatCoreEmoticonsKeyString;
}

+ (NSString*)regularExpressionString {
  return @"(?<=\\()\\w{1,15}(?=\\))";
}

@end
