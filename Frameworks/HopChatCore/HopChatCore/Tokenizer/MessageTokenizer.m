//
//  MessageTokenizer.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/1/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "MessageTokenizer.h"
#import "NSArray+ATLAdditions.h"
#import "MentionToken.h"
#import "EmoticonToken.h"
#import "LinkToken.h"

@interface MessageTokenizer ()

@property (nonatomic, copy, readwrite) NSArray<Class>* tokenClasses;

@end

@implementation MessageTokenizer

#pragma mark - Object Lifecycle

- (instancetype)init {
  NSArray* tokenClasses = @[MentionToken.class, EmoticonToken.class, LinkToken.class];
  return [self initWithTokenClasses:tokenClasses];
}

- (instancetype)initWithTokenClasses:(NSArray<Class>*)tokenClasses {
  self = [super init];
  if (self) {
    _tokenClasses = [tokenClasses copy];
  }
  return self;
}

#pragma mark - Public Interface

- (NSArray<id<Token>>*)tokenize:(NSString*)message {
  NSParameterAssert(message);
  NSMutableArray* tokens = [NSMutableArray array];

  return [self.tokenClasses atl_reduceWithInitialValue:tokens
      block:^id(Class tokenClass, NSUInteger idx, NSMutableArray* tokens) {
        [tokens addObjectsFromArray:[self tokensInString:message forTokenClass:tokenClass]];
        return tokens;
      }];
}

#pragma mark - Private

- (NSArray<id<Token>>*)tokensInString:(NSString*)string forTokenClass:(Class)tokenClass {
  NSRange stringRange = NSMakeRange(0, string.length);
  NSArray* matches = [[tokenClass regularExpression] matchesInString:string options:kNilOptions range:stringRange];

  return [matches atl_mapObjectsUsingBlock:^id(NSTextCheckingResult* match, NSUInteger idx) {
    NSString* rawContent = [string substringWithRange:match.range];
    NSString* content = [self contentFromRawContent:rawContent match:match forTokenClass:tokenClass];
    return [[tokenClass alloc] initWithRange:match.range rawContent:rawContent content:content];
  }];
}

- (NSString*)contentFromRawContent:(NSString*)rawContent
                             match:(NSTextCheckingResult*)match
                     forTokenClass:(Class)tokenClass {
  switch ([tokenClass type]) {
    case MessageTokenTypeText: {
      if (tokenClass == MentionToken.class) {
        return [rawContent substringFromIndex:1]; // removing @ in the beginning
      }
    }
    case MessageTokenTypeLink: {
      return match.URL.absoluteString;
    }
    case MessageTokenTypeNA: {
      NSAssert(NO, @"Must not be reached");
      break;
    }
  }
  return nil;
}

@end
