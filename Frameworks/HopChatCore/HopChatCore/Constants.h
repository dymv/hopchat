//
//  Constants.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/4/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

FOUNDATION_EXPORT NSString* const HopChatCoreURLKeyString;
FOUNDATION_EXPORT NSString* const HopChatCoreTitleKeyString;
FOUNDATION_EXPORT NSString* const HopChatCoreEmoticonsKeyString;
FOUNDATION_EXPORT NSString* const HopChatCoreMentionsKeyString;
FOUNDATION_EXPORT NSString* const HopChatCoreLinksKeyString;

@end
