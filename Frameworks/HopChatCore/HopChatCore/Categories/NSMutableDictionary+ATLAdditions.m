//
//  NSMutableDictionary+FunctionalAdditions.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "NSMutableDictionary+ATLAdditions.h"

@implementation NSMutableDictionary (ATLAdditions)

- (void)atl_appendObject:(id)object forKey:(id<NSCopying>)key {
  NSMutableArray* array = self[key];

  if (!array) {
    array = [NSMutableArray array];
    self[key] = array;
  }

  [array addObject:object];
}

@end
