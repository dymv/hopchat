//
//  NSArray+ATLAdditions.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (ATLAdditions)

- (NSArray*)atl_mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block;
- (id)atl_reduceWithInitialValue:(id)value block:(id (^)(id obj, NSUInteger idx, id memo))block;

@end

NS_ASSUME_NONNULL_END
