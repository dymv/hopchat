//
//  NSMutableDictionary+ATLAdditions.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableDictionary (ATLAdditions)

- (void)atl_appendObject:(id)object forKey:(id<NSCopying>)key;

@end

NS_ASSUME_NONNULL_END
