//
//  NSArray+ATLAdditions.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "NSArray+ATLAdditions.h"

@implementation NSArray (ATLAdditions)

- (NSArray*)atl_mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block {
  NSMutableArray* result = [NSMutableArray arrayWithCapacity:self.count];

  [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop) {
    [result addObject:block(obj, idx)];
  }];

  return [result copy];
}

- (id)atl_reduceWithInitialValue:(id)value block:(id (^)(id obj, NSUInteger idx, id memo))block {
  __block id currentMemo = value;

  [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop) {
    currentMemo = block(obj, idx, currentMemo);
  }];
  
  return currentMemo;
}

@end
