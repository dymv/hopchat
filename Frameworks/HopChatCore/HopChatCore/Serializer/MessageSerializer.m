//
//  MessageSerializer.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/2/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "MessageSerializer.h"
#import "NSMutableDictionary+ATLAdditions.h"
#import "BaseToken.h"
#import "MessageTokenizer.h"
#import "URLInfoFetcher.h"
#import "URLInfo.h"
#import "Constants.h"

@interface MessageSerializer ()

@property (nonatomic, strong) MessageTokenizer* tokenizer;
@property (nonatomic, strong) URLInfoFetcher* urlInfoFetcher;

@property (nonatomic, strong) NSOperationQueue* workQueue;

@end

@implementation MessageSerializer

#pragma mark - Object Lifecycle

- (instancetype)init {
  return [self initWithTokenizer:[[MessageTokenizer alloc] init]
                  urlInfoFetcher:[[URLInfoFetcher alloc] init]];
}

- (instancetype)initWithTokenizer:(MessageTokenizer*)tokenizer urlInfoFetcher:(URLInfoFetcher*)urlInfoFetcher {
  self = [super init];
  if (self) {
    _tokenizer = tokenizer;
    _urlInfoFetcher = urlInfoFetcher;

    _workQueue = [[NSOperationQueue alloc] init];
    _workQueue.maxConcurrentOperationCount = 1;
  }
  return self;
}

#pragma mark - Public Interface

- (void)serialize:(NSString*)message withCompletion:(MessageSerializerCompletion)completion {
  NSParameterAssert(message);
  NSParameterAssert(completion);

  [self.workQueue addOperationWithBlock:^{
    NSMutableDictionary* representations = [NSMutableDictionary dictionary];
    NSArray* tokens = [self.tokenizer tokenize:message];
    __block NSError* serializationError;

    [tokens enumerateObjectsWithOptions:NSEnumerationConcurrent
        usingBlock:^(id<Token> token, NSUInteger idx, BOOL* stop) {
         NSError* inSerializationError;
         id serialized = [self serializedToken:token error:&inSerializationError];
         @synchronized (self) {
           [representations atl_appendObject:serialized
                                      forKey:[token.class category]];
           if (inSerializationError) {
             serializationError = inSerializationError;
           }
         }
        }];

    if (serializationError) {
      completion(nil, serializationError);
      return;
    }

    NSError* jsonizationError;
    NSData* data = [NSJSONSerialization dataWithJSONObject:representations
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:&jsonizationError];
    NSString* serializedMessage = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

    if (jsonizationError) {
      completion(nil, jsonizationError);
      return;
    }

    completion(serializedMessage, nil);
  }];
}

#pragma mark - Private

- (id)serializedToken:(id<Token>)token error:(NSError* __autoreleasing *)error {
  switch ([token.class type]) {
    case MessageTokenTypeText:
      return token.content;
    case MessageTokenTypeLink:
      return [self linkTokenRepresentation:token error:error];
    case MessageTokenTypeNA:
      NSAssert(NO, @"Must not be reached");
      break;
  }
  return nil;
}

- (NSDictionary*)linkTokenRepresentation:(id<Token>)token error:(NSError* __autoreleasing *)error {
  NSURL* url = [NSURL URLWithString:token.content];
  NSString* title = [self.urlInfoFetcher fetchInfoForURL:url error:error].title;
  return @{ HopChatCoreURLKeyString: token.content, HopChatCoreTitleKeyString: title ?: @"" };
}

@end
