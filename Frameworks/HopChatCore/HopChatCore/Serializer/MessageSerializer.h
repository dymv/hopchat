//
//  MessageSerializer.h
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/2/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MessageTokenizer;
@protocol URLInfoFetcher;

NS_ASSUME_NONNULL_BEGIN

typedef void (^MessageSerializerCompletion)(NSString* _Nullable serialized, NSError* _Nullable error);

@protocol MessageSerializer <NSObject>

- (void)serialize:(NSString*)message withCompletion:(MessageSerializerCompletion)completion;

@end

@interface MessageSerializer : NSObject <MessageSerializer>

- (instancetype)initWithTokenizer:(id<MessageTokenizer>)tokenizer
                   urlInfoFetcher:(id<URLInfoFetcher>)urlInfoFetcher NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
