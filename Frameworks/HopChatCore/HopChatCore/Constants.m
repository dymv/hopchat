//
//  Constants.m
//  HopChatCore
//
//  Created by Dymov, Eugene (Agoda) on 12/4/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString* const HopChatCoreURLKeyString = @"url";
NSString* const HopChatCoreTitleKeyString = @"title";
NSString* const HopChatCoreEmoticonsKeyString = @"emoticons";
NSString* const HopChatCoreMentionsKeyString = @"mentions";
NSString* const HopChatCoreLinksKeyString = @"links";

@end
