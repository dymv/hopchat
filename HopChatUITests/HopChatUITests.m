//
//  HopChatUITests.m
//  HopChatUITests
//
//  Created by Dymov, Eugene (Agoda) on 12/3/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface HopChatUITests : XCTestCase

@end

@implementation HopChatUITests

- (void)setUp {
  [super setUp];
  self.continueAfterFailure = NO;
  [[[XCUIApplication alloc] init] launch];
}

- (void)tearDown {
  [super tearDown];
}

- (void)testMessageSerialization {
  XCUIApplication *app = [[XCUIApplication alloc] init];
  XCUIElement *messagetextviewTextView = app.textViews[@"messageTextView"];
  [messagetextviewTextView tap];
  [messagetextviewTextView typeText:@"Testing, @mention (emoticon) apple.com url"];

  XCUIElement* sendButton = app.buttons[@"sendButton"];
  [sendButton tap];

  NSPredicate* buttonEnabled = [NSPredicate predicateWithFormat:@"enabled == true"];
  [self expectationForPredicate:buttonEnabled evaluatedWithObject:sendButton handler:nil];
  [self waitForExpectationsWithTimeout:5 handler:nil];

  NSString* actualOutput = app.textViews[@"outputTextView"].value;
  NSString* path = [[NSBundle bundleForClass:self.class] pathForResource:@"expected-output" ofType:@"txt"];
  NSString* expectedOutput = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
  XCTAssertEqualObjects([self parsed:actualOutput], [self parsed:expectedOutput]);
}

- (NSString*)trimmed:(NSString*)string {
  return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSDictionary*)parsed:(NSString*)json {
  return [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
}

@end
